const slider = document.getElementById('userSlider');
const sliderValue = document.getElementById('sliderValue');
const pricingButtons = document.querySelectorAll('.price-btn');

slider.addEventListener('input', () => {
  const numberOfUsers = parseInt(slider.value, 10);
  sliderValue.textContent = `${numberOfUsers} users`;
  highlightPlan(numberOfUsers);
});

function highlightPlan(numberOfUsers) {
  pricingButtons.forEach((button, index) => {
    const minUsers = index * 10;
    const maxUsers = (index + 1) * 10;

    if (numberOfUsers > minUsers && numberOfUsers <= maxUsers) {
      button.classList.add('btn-primary');
      button.classList.remove('btn-outline-primary');
    } else {
      button.classList.remove('btn-primary');
      button.classList.add('btn-outline-primary');
    }
  });
}



