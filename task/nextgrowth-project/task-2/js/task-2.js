
const productsContainer = document.getElementById('products-container');
const loadMoreButton = document.getElementById('load-more-btn');
let currentPage = 1;

async function fetchProducts() {
  const response = await fetch(`https://jsonplaceholder.typicode.com/photos?_page=${currentPage}&_limit=6`);
  const data = await response.json();
  return data;
}

async function displayProducts() {
  const products = await fetchProducts();
  products.forEach(product => {
    const productMainDiv = document.createElement('div');
    productMainDiv.classList.add('col-12', 'col-md-6', 'col-lg-4');
    productDiv = document.createElement('div');
    productDiv.classList.add('card');
    productDiv.innerHTML = `<img src='${product.url}' class='img-fluid'><h5>${product.title}</h5>`;
    productMainDiv.appendChild(productDiv);
    productsContainer.appendChild(productMainDiv);
  });
  currentPage++;
}

async function checkAndLoadMore() {
  const scrollPosition = window.innerHeight + window.scrollY;
  const documentHeight = document.body.offsetHeight;

  if (scrollPosition >= documentHeight) {
    await displayProducts();
  }
}

/*loadMoreButton.addEventListener('click', displayProducts);*/

window.addEventListener('scroll', async () => {
  await checkAndLoadMore();
});

// Initial load
displayProducts();
